﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product3.Model
{
    internal class Product
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public int Price { get; set; }
        public float Rating { get; set; }

        public Product(string name, string category, int price, float rating)
        {
            Name = name;
            Category = category;
            Price = price;
            Rating = rating;
        }

        public override string ToString()
        {
            return $"Product Name: {Name}, Product Category: {Category}, Product Price: {Price}, Product Rating: {Rating}";
        }
    }
}
