﻿using Product3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product3.ProductRepository
{
    internal class ProductRepository
    {
        Product[] products;

        public ProductRepository()
        {
            products = new Product[]
            {
                new Product("Oppo","Mobile",15000,4.2f),
                new Product("Redmi", "Mobile", 20000, 1.2f),
                new Product("Thinkpad", "Laptop", 55000, 43.2f),
                new Product("Dell", "Laptop", 65000, 3.2f)
        };
        }

        public Product[] GetAllProducts()
        {
            return products;

        }

        public Product[] GetProductsByCategory(string category)
        {
            return Array.FindAll(products, product => product.Category == category);
        }

        public Product[] UpdateDetails(string name, int price)
        {
            foreach(Product value in products)
            {
                if(value.Category == name)
                {
                    value.Price = price;
                }
               
            }
            return products;
        }

        public Product[] DeleteProduct(string name)
        {
            return Array.FindAll(products, product => product.Name != name);
        }
    }
    }
