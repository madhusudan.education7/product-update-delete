﻿// See https://aka.ms/new-console-template for more information
using Product3.Model;
using Product3.ProductRepository;

ProductRepository productRepository = new ProductRepository();
Product[] allProducts = productRepository.GetAllProducts();
//foreach(Product item in allProducts)
//{
//    Console.WriteLine($"Product Name: {item.Name}");
//}

//Console.WriteLine(productRepository.GetProductsByCategory("Mobile"));
//Product[] mobiles = productRepository.GetProductsByCategory("Mobile");
//foreach (Product item in mobiles)
//{
//    Console.WriteLine(item);
//}

Product[] details = productRepository.UpdateDetails("Mobile", 1000);
foreach (Product value in details)
{
    //Console.WriteLine("Updated value");
    Console.WriteLine(value);
}

Console.WriteLine("----------------------------------");

Product[] deleteProduct = productRepository.DeleteProduct("Oppo");
foreach(Product item in deleteProduct)
{
    //Console.WriteLine("Deleted value");
    Console.WriteLine(item);
}
