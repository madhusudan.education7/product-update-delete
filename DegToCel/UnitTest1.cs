namespace DegToCel
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var givenTemp = 45.66;
            var expectedTemp = 114.188;

            var actual = TempConvertor.CelsiusToFah(givenTemp);
            Assert.Equal(expectedTemp, actual);
        }

        

    }
}