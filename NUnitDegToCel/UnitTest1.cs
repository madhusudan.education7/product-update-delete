namespace NUnitDegToCel
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var givenTemp = 45.66;
            var expectedTemp = 114.188;

            var actual = TempConvertor.CelsiusToFah(givenTemp);
            Assert.AreEqual(expectedTemp, actual);
        }
    }
}